#
# from numpy import np
#
# # Creating array object
# arr = np.array([[1, 2, 3],
#                 [4, 2, 5],
#                 [3 ,5 ,6]])
#
# # Printing type of arr object
# print("Array is of type: ", type(arr))
#
# # Printing array dimensions (axes)
# print("No. of dimensions: ", arr.ndim)
#
# # Printing shape of array
# print("Shape of array: ", arr.shape)
#
# # Printing size (total number of elements) of array
# print("Size of array: ", arr.size)
#
# # Printing type of elements in array
# print("Array stores elements of type: ", arr.dtype)
#
# # Python program to demonstrate
# arr = array([1,2.0,3,4,5], int)
# print(arr.dtype)
# print(arr)
#
# arr1 = linspace(2, 17, 16)
# print(arr1)
#
# arr2 = arange(2, 17)
# print(arr2)
#
# arr3 = logspace(1, 40)
# print(arr3[10])
#
# arr4 = zeros(5, int)
# print(arr4)
#
# arr5 = ones(5, int)
# print(arr5)
# for i in range(5, 0, -1):
#     for j in range(i):
#         print(i, end=" ")
#
#     print()
#
# from numpy import *
# #
# arr = array([0, 15], float)
# print(arr)
#
# arr1 = linspace([0, 15], 2)
# print(arr1)
#
# import numpy as np
#
# # Create an empty array
# empa = np.empty((3, 4), dtype=int)
# print("Empty Array")
# print(empa, "\n")
#
# # Create a full array
# flla = np.full([3, 3], 55, dtype=float)
# print("\nFull Array")
# print(flla)
#
# pilla = np.full([4,4], 7, dtype= str)
# print("\nFull Array")
# print(pilla)
#
# chilla = np.full([2,2], 108, dtype= int)
# print("\nFull Array")
# print(chilla)
#
# hilla = np.full([4,4], 6, dtype= float)
# print("\n full mat")
# print(hilla)
#
# jilla = np.full([5,5], 8, dtype= str)
# print("\n full matr")
# print(jilla)
#
# killa = np.empty((4,4), dtype= int)
# print(killa)
#
# print("मुझे काम करने के लिए जाना है")
# for i in range(6, 0, -1):
#     for j in range(i):
#         print(" ", end="")
#
#     for k in range(6-i):
#         print(2 * "*", end="")
#
#     print()
#
# for i in range(6):
#     for j in range(i):
#         print(" ", end="")
#
#     for k in range(6-i):
#         print(2 * "*", end="")
#
#     print()
#
# for i in range(5):
#     for j in range(5):
#         print(" ", end="")
#
#     for k in range(2):
#         print("*", end="")
#
#     print()
#
# for i in range(4):
#     for j in range(4):
#
#         print(" ", end="")
#     for k in range(4, 8):
#         print("*", end="")
#     # # #
#     print()
#
# print()
#
# for i in range(5, -1, -1):
#     for j in range(i):
#         print(" ", end=" ")
#
#     for j in range(7-(2 * i)):
#         print(("*"), end=" ")
#
#     print()
#
# for i in range(2, -1, -1):
#     for j in range(i):
#         print(" ", end=" ")
#
#     for j in range(7-(2 * i)):
#         print(("*"), end=" ")
#
#     print()
# for i in range(2, -1, -1):
#     for j in range(i):
#         print(" ", end=" ")
#
#     for j in range(7-(2 * i)):
#         print(("*"), end=" ")
#
#     print()
#
# for i in range(3):
#     for j in range(4):
#
#         print(" ", end="")
#     for k in range(5, 8):
#         print("*", end=" ")
#
#     print()
#
#
# for i in range(3):
#     for j in range(i):
#         print(" ", end=" ")
#
#     for j in range(7-(2 * i)):
#         print(("*"), end=" ")
#
#     print()
# print("  ",3 * " *")
#
# print()
#
# for i in range(1, 7):
#     for j in range(7-i,1,-1):
#         print(" ", end="")
#
#     print((i) * "*")
#
# print()
#
# for i in range(3):
#     for j in range(2-i):
#         print(" ", end=" ")
#
#     print(((4 * i) + 1) * "*", end="")
#     print()
#
# for i in range(2, -1, -1):
#     for j in range(3-i, -1, -1):
#         print(" ", end="")
#
#     print(((2 * i) + 1) * "*", end=" ")
#     print()
#
# for i in range(5):
#     for j in range(i):
#         print(" ", end="")
#
#     for j in range(5-i):
#         print("*", end="")
#
#     for j in range(i):
#         print(" ", end="")
#
#     for j in range(5-i):
#         print("*", end="")
#     print()
#
# for i in range(2):
#     print(3 * " ", "*")

import pandas as pd

# mydataset = {'cars': ("BMW", "Volvo", "Ford"), 'passings': (3, 7, 2), "pasengers" : [2,3,4]}
#
# myvar = pandas.DataFrame(mydataset)
#
# print(myvar)
#
# university = {"school" : ["silver", "aps", "lords"], "way" : ("north", "south", "east")}
#
# w = pd.DataFrame(university)
#
# print(w)
#
# print(pd.__version__)
#
# r = ["NITISH","RISHABH","VARUNM"]
#
# print(pd.Series(r))
# print(r[0])
#
# m = pd.Series(r, index = ["x", "y", "z"])
#
# print(m)
#
# print(m["x"])
#
# calories = {"day1": 420, "day2": 380, "day3": 390}
#
# e = pd.Series(calories)
#
# print(pd.Series(calories, index = ["day1", "day2"]))

# data = {
#   "calories": [420, 380, 390],
#   "duration": [50, 40, 45]
# }
#
# myvar = pd.DataFrame(data)
#
# print(myvar.loc[[0]])
#
# # t = pd.read_csv('data.csv')
# # print(t)
# pd.options.display.max_rows = 9999
# # pd.options.display.max_rows = 9999
#
# print(pd.options.display.max_rows)


# df = pd.read_json('data.json')
#
# print(df.to_string())

# r = ["cars", "price", "range"]
# t = [5, 300000, 500000]
# u = [5, 250000, 500000]
#
# a = {"name" : r, "range" : t, "up" : u}
#
# w = pd.Series(a)
#
# w.to_csv('/home/consolebit/Downloads/file1.csv')

# r = 32
# sum = 0
# rev = 0
# while r != 0:
#     sum = (sum) + (r % 10)
#     r = r // 10
#
# print(sum)

import numpy as np
#
# a = [[2,1,0],
#      [1,2,1],
#      [0,1,1]]
#
# t = []
# for i in range(len(a)):
#     for j in range(len(a[i])):
#         if a[i][j] != 2 and a[i][j] != 0:
#             a[i][j] = 2
#
# print(np.array(a))

m = [   [1, 1, 1, 1, 1, 1, 0],
        [1, 1, 0, 0, 1, 1, 1],
        [1, 1, 0, 1, 0, 1, 1],
        [1, 1, 1, 0, 0, 1, 0],
        [1, 0, 1, 1, 1, 0, 0],
        [1, 0, 0, 0, 1, 1, 1]
                                 ]
temp = 0
sum = 0
for i in range(len(m)):
    for j in range(len(m[i])):
        sum = sum + m[i][j]
        if m[i][j] == 0:
            temp = m.index(m[i][j])
            break


print(sum)
print(temp)


