class parent:

    behaviour = "calm"
    mentality = "free"

    def __init__(self, personality, children):
        self.personality = personality
        self.children = children

    def func(self):
        return self.personality

    def func1(self):
        return self.children, True

    @staticmethod
    def info8(s):
        return s[::-1]

class child(parent):
    def __init__(self, personality, children, age, carrier):
        self.age = age
        self.carrier = carrier

        parent.__init__(self, personality, children)

    def func2(self):
        return "aa to gaya hun bc"

    def func3(self):
        return "True everything is possible no matter how hard is it"

    @staticmethod
    def sorte(l1):
        r = []
        while l1:
            max = l1[0]
            for i in l1:
                if i > max:
                    max = i

            r.append(max)
            l1.remove(max)

        for j in r:
            print(j)

        return "sorted in the descending order", r

a = child("nice", 4, 50, "retired")
print(a.personality, a.children, a.age, a.carrier)

class grandchild(child):

    def __init__(self, personality, children, age, carrier, name, grandage):
        self.name = name
        self.grandage = grandage

        child.__init__(self, personality, children, age, carrier)

    def info7(n):
        rev = 0
        num = n

        while num != 0:
            rev = (rev * 10) + (num % 10)
            num = num // 10

        return int(rev)

    @staticmethod
    def sort(l):
        a = []
        while l:
            min = l[0]
            for i in l:
                if i < min:
                    min = i

            a.append(min)
            l.remove(min)

        return "sorted", a

b = grandchild("nice", 4, 50, "retired", "shreyansh", 10)
print(b.personality, b.children, b.age, b.carrier, b.name, b.grandage)

print(b.func(), b.func1())

print(b.func2(), b.func3())

class greatgrand(grandchild):

    def __init__(self, personality, children, age, carrier, name, grandage, greatgrandson, ggname):
        self.greatgrandson = greatgrandson
        self.ggname = ggname

        grandchild.__init__(self, personality, children, age, carrier, name, grandage )

    def func5(self):
        return "believe in yourself"

    def func6(self):
        return """getting older and accepting the situations of the life and fighting"""

    def  avg(self, m1, m2 , m3):
        self.m1 = m1
        self.m2 = m2
        self.m3 = m3

        return (m1 + m2 + m3)/3

    def get_m1(self):
        return self.m1

    def set_m1(self, val):
        self.m1 = val
        return self.m1

    @classmethod
    def info(cls, val):
        cls.behaviour = val
        return cls.behaviour

    @classmethod
    def info1(cls):
        return cls.mentality

    @staticmethod
    def info3(n):
        while n != 0:
            product = 1
            for i in range(1, n + 1):
                  product = product * i

            n = n - 1
            return product

    @staticmethod
    def info4(d):
        a = []

        for i in d:
            a.append((i, d[i][0].upper() + d[i][1:]))

        return dict(a)

    @staticmethod
    def info5(d):
        r = []
        t = []
        y = []
        q = []
        for i in d:
            r.append(i)
            t.append(d[i])

        for i in t:
            i = list(i)
            i.insert(3, "5")
            y.append(i)

        for u in y:
            q.append("".join(u))

        return dict(zip(r, q))

    @staticmethod
    def fab(n):
          current_number = 1
          previous_number = 0
          sum = 0

          for i in range(n):
             sum = current_number + previous_number
             previous_number = current_number
             current_number = sum

             print(sum)

b = greatgrand("nice", 4, 50, "retired", "shreyansh", 10,"harshit", "mukesh")
b1 = greatgrand("nice", 4, 50, "retired", "shreyansh", 10,"harshit", "mukesh")

b.name = "rishabh"
b.children = 5
b.personality = "good"
b.ggname = "varun"
b.age = 80
b.carrier = "builder"
b.greatgrandson = "aashish"

b.mentality = "harsh"
b.behaviour = "strict"



print(b.func5(), b.func6())
print(b.personality, b.children, b.age, b.carrier, b.name, b.grandage, b.greatgrandson, b.ggname, b.behaviour, b.mentality)
print(int((b.avg(22,34,65))))
print(int(b1.avg(56, 96, 67)))
print(b1.m1)
print(b1.get_m1())
print(b.set_m1(4))
print(greatgrand.info("angryallthetime"))
print(greatgrand.info1())
print(greatgrand.info1())
print(greatgrand.info3(5))
print(greatgrand.info4({"country" : "india", "company" : "consolebit"}))
print(greatgrand.info5({"country" : "india", "company" : "consolebit"}))
print(grandchild.info7(4567))
print(parent.info8("rishabh"))
print(b.sort([3,6,2,0,6,9,1,3,2,45]))
print(b.sorte([0,3,1,2,5,7,9,8,5]))
print(b.fab(10))