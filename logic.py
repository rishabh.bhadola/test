# d = {"name" : "rishabh", "name2" : "varun", "name3" : "nitish"}
#
# t = []
# h = []
# u = []
# for i in d:
#     t.append(i)
#     h.append(d[i])
#
# print(t)
# # print(h)
#
# import numpy as np
#
# arr = np.array((1, 2, 3, 4, 5))
#
# print(arr)
#
# print(type(arr))

import numpy as np

# arr = np.array(42)
#
# print(arr)

# arr = np.array([[[1, 2, 3], [3,2,4]], [[2,3,4], [4,3,5]]])
#
# print(arr)

# arr = np.array([[[1, 2, 3], [4, 5, 6]], [[1, 2, 3], [4, 5, 6]]])
#
# print(arr)
#
# a = np.array(42)
# b = np.array([1, 2, 3, 4, 5])
# c = np.array([[1, 2, 3], [4, 5, 6]])
# d = np.array([[[1, 2, 3], [4, 5, 6]], [[1, 2, 3], [4, 5, 6]]])
#
# print(a.ndim)
# print(b.ndim)
# print(c.ndim)
# print(d.ndim)
#
# arr = np.array([1, 2, 3, 4], ndmin=5)
#
# print(arr)
# print('number of dimensions :', arr.ndim)

# arr = np.array([1, 2, 3, 4])
#
# print(arr[2] + arr[3])

# arr = np.array([[1,2,3,4,5], [6,7,8,9,10]])
#
# print('2nd element on 1st row: ', arr[0, 1])

# arr = np.array([[[1, 2, 3], [4, 5, 6]], [[7, 8, 9], [10, 11, 12]]])
#
# print(arr[0, 1, 2])

# arr = np.array([[1,2,3,4,5], [6,7,8,9,10]])
#
# print('Last element from 2nd dim: ', arr[1, -1])

# arr = np.array([[1,2,3], [2,3,4]])
# print(arr)
#
# arr1 = np.empty((3,3))
# print(arr1)
#

# import array
# a = [4, 6, 8, 3, 1, 7]
# print(a[-3])
# print(a[-5])
# print(a[-1])

# import random
# print(random.uniform(2,3))
#
# sum = 0
# for i in range(102):
#     sum = sum + i
#
# print(sum)
# global_var = 0
# def modify_global_var():
#     global global_var # Setting global_var as a global variable
#     global_var = 10
#     print(global_var)
# print(modify_global_var)

# arr = np.array([1, 2, 3, 4, 5, 6, 7])
#
# print(arr[1:5])
#
# arr = np.array([1, 2, 3, 4, 5, 6, 7])
#
# print(arr[-3:-1])


# arr = np.array([1, 2, 3, 4, 5, 6, 7])
#
# print(arr[1:5:2])

# arr = np.array([1, 2, 3, 4, 5, 6, 7])
#
# print(arr[::2])


# arr = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
#
# print(arr[1, 1:4])

# arr = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
#
# print(arr[0:2, 2])

# arr = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])
#
# print(arr[0:2, 1:4])

# arr = np.array([1, 2, 3, 4])
#
# print(arr.dtype)
#
# arr = np.array(['apple', 'banana', 'cherry'])
#
# print(arr.dtype)

# arr = np.array([1, 2, 3, 4], dtype='S')
#
# print(arr)
# print(arr.dtype)

# arr = np.array([1,2,3,4,5])
# q = arr.view()
# arr[0] = 42
#
# print(arr)
# print(q)

#
# arr = np.array([1, 2, 3, 4, 5])
# x = arr.view()
# x[0] = 31
#
# print(arr)
# print(x)

# arr = np.array([1, 2, 3, 4, 5])
#
# x = arr.copy()
# y = arr.view()
#
# print(x.base)
# print(y.base)

# arr = np.array([1, 2, 3, 4], ndmin=5)
#
# print(arr)
# print('shape of array :', arr.shape)

# arr = np.array([1, 2, 3, 4, 5, 6, 7, 8,4])
#
# newarr = arr.reshape(3, 3)
#
# print(newarr)

# arr = np.array([1, 2, 3, 4, 5, 6, 7, 8])
#
# print(arr.reshape(2, 4).base)

# arr = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
#
# newarr = arr.reshape((2, 3, 2))
#
# print(newarr)

# arr = np.array([1, 2, 3, 4, 5, 6, 7, 8])
#
# newarr = arr.reshape((2, 2, -1))
#
# print(newarr)

#
# arr = np.array([1, 2, 3])
#
# for x in arr:
#   print(x)
#
# arr = np.array([[1, 2, 3], [4, 5, 6]])
#
# for x in arr:
#   print(x)

# arr = np.array([[1, 2, 3], [4, 5, 6]])
#
# for x in arr:
#   for y in x:
#     print(y)

# arr = np.array([[[1, 2, 3], [4, 5, 6]], [[7, 8, 9], [10, 11, 12]]])
#
# for x in arr:
#   print(x)

# arr = np.array([[[1, 2, 3], [4, 5, 6]], [[7, 8, 9], [10, 11, 12]]])
#
# for x in arr:
#   for y in x:
#     for z in y:
#       print(z)

# arr = np.array([[[1, 2], [3, 4]], [[5, 6], [7, 8]]])
#
# for x in np.nditer(arr):
#   print(x)

# arr1 = np.array([1, 2, 3])
#
# arr2 = np.array([4, 5, 6])
#
# arr = np.concatenate((arr1, arr2))
#
# print(arr)

# arr1 = np.array([[1, 2], [3, 4]])
#
# arr2 = np.array([[5, 6], [7, 8]])
#
# arr = np.concatenate((arr1, arr2), axis=1)
#
# print(arr)


# arr1 = np.array([1, 2, 3])
#
# arr2 = np.array([4, 5, 6])
#
# arr = np.stack((arr1, arr2), axis=1)
#
# print(arr)

# arr1 = np.array([1, 2, 3])
#
# arr2 = np.array([4, 5, 6])
#
# arr = np.hstack((arr1, arr2))
#
# print(arr)

# arr1 = np.array([1, 2, 3])
#
# arr2 = np.array([4, 5, 6])
#
# arr = np.vstack((arr1, arr2))
#
# print(arr)
#
#
# arr1 = np.array([1, 2, 3])
#
# arr2 = np.array([4, 5, 6])
#
# arr = np.dstack((arr1, arr2))
#
# print(arr)

# arr = np.array([1, 2, 3, 4, 5, 6])
#
# newarr = np.array_split(arr, 3)
#
# print(newarr)

# arr = np.array([1, 2, 3, 4, 5, 6])
#
# newarr = np.array_split(arr, 4)
#
# print(newarr)

# arr = np.array([1, 2, 3, 4, 5, 6])
#
# newarr = np.array_split(arr, 3)
#
# print(newarr[0])
# print(newarr[1])
# print(newarr[2])

# arr = np.array([[1, 2], [3, 4], [5, 6], [7, 8], [9, 10], [11, 12]])
#
# newarr = np.array_split(arr, 3)
#
# print(newarr)

# arr = np.array([1, 2, 3, 4, 5, 4, 4])
#
# x = np.where(arr == 4)
#
# print(x)

# arr = np.array([1, 2, 3, 4, 5, 6, 7, 8])
#
# x = np.where(arr%2 == 0)
#
# print(x)

#
# arr = np.array([6, 7, 8, 9])
#
# x = np.searchsorted(arr, 7)
#
# print(x)

# arr = np.array([6, 7, 8, 9])
#
# x = np.searchsorted(arr, 7, side='right')
#
# print(x)

#
# arr = np.array([1, 3, 5, 7])
#
# x = np.searchsorted(arr, [2, 4, 6])
#
# print(x)

# arr = np.array([3, 2, 0, 1])
#
# print(np.sort(arr))

# arr = np.array(['banana', 'cherry', 'apple'])
#
# print(np.sort(arr))

# arr = np.array([True, False, True])
#
# print(np.sort(arr))

# arr = np.array([[3, 2, 4], [5, 0, 1]])
#
# print(np.sort(arr))

# arr = np.array([41, 42, 43, 44])
#
# x = [True, False, True, False]
#
# newarr = arr[x]
#
# print(newarr)

# arr = np.array([41, 42, 43, 44])
#
# # Create an empty list
# filter_arr = []
#
# # go through each element in arr
# for element in arr:
#   # if the element is higher than 42, set the value to True, otherwise False:
#   if element > 42:
#     filter_arr.append(True)
#   else:
#     filter_arr.append(False)
#
# newarr = arr[filter_arr]
#
# print(filter_arr)
# print(newarr)

# arr = np.array([1, 2, 3, 4, 5, 6, 7])
#
# # Create an empty list
# filter_arr = []
#
# # go through each element in arr
# for element in arr:
#   # if the element is completely divisble by 2, set the value to True, otherwise False
#   if element % 2 == 0:
#     filter_arr.append(True)
#   else:
#     filter_arr.append(False)
#
# newarr = arr[filter_arr]
#
# print(filter_arr)
# print(newarr)


# arr = np.array([41, 42, 43, 44])
#
# filter_arr = arr > 42
#
# newarr = arr[filter_arr]
#
# print(filter_arr)
# print(newarr)

