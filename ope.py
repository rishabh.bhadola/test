class student:

    def __init__(self, m1, m2):
        self.m1 = m1
        self.m2 = m2

    def __add__(self, s2):
        m1 = self.m1 + s2.m1
        m2 = self.m2 + s2.m2

        s3 = student(m1, m2)
        return s3

    def __mul__(self, s2):
        m1 = self.m1 * s2.m1
        m2 = self.m2 * s2.m2

        s4 = student(m1, m2)

        return s4

    def __sub__(self, s2):
        m1 = s2.m1 - self.m1
        m2 = s2.m2 - self.m2

        s5 = student(m1, m2)

        return s5

    def __gt__(self, other):
         r1  = self.m1 + self.m2
         r2 = other.m1 + other.m2

         if r1 > r2:
             return True
         else:
             return False

    def __str__(self):
        return "{} {} ".format(self.m1, self.m2)

s1 = student(11, 9)
s2 = student(20, 40)

s3 = s1 + s2

s4 = s1 * s2

s5 = s1 - s2

print(s4.m1, s4.m2)

print(s5.m1, s5.m2)

if s1 > s2:
    print(s1)

else:
    print(s2)